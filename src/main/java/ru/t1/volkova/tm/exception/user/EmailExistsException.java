package ru.t1.volkova.tm.exception.user;

public final class EmailExistsException extends AbstractUserException {

    public EmailExistsException() {
        super("Error! Email already exists...");
    }

}
