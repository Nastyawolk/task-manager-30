package ru.t1.volkova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.enumerated.Role;

public final class DataBackupSaveCommand extends DataBase64SaveCommand {

    @NotNull
    public static final String DESCRIPTION = "Save backup to file";

    @NotNull
    public static final String NAME = "backup-save";

    @SneakyThrows
    @Override
    public void execute() {
        setFile_name(FILE_BACKUP);
        super.execute();
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
